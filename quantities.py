import sys

items = {}


def op_add():
    try:
        item = sys.argv[0]
        can = int(sys.argv[1])
    except ValueError:
        sys.exit("Las cantidades deben de ser numeros enteros")

    if items.get(item):
        items[item] = can
    else:
        items.setdefault(item, can)


def op_items():
    lista = []
    if len(items) != 0:
        for x in items.keys():
            lista.append(x)
        lista_items = " ".join(lista)
        print("Objetos: ",lista_items)
    else:
        print("No hay este objeto")


def op_all():
    lista_item = []
    lista_cantidad = []
    if len(items) != 0:
        for item, cantidad in items.items():
            mensaje=f"{item} ({cantidad})"
            lista_item.append(mensaje)
        item_list = " ".join(lista_item)
        print("All: ",item_list)
    else:
        print("No hay ningun objeto")


def op_sum():
    cantidad = items.values()
    suma = 0
    for x in cantidad:
        suma = suma + x

    print("Sum: ",suma)


def main():

    while sys.argv:
        header = sys.argv.pop(0)
        if header == "add":
            op_add()
        elif header == "items":
            op_items()
        elif header == "all":
            op_all()
        elif header == "sum":
            op_sum()

if __name__ == '__main__':
    main()
